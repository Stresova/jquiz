package com.jquiz.model;

import com.jquiz.service.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Quiz model
 */
public class Quiz {
    private static final Integer LAST_QUIZZES_COUNT = 6;
    private static final String FIND_LAST_QUIZZES_QUERY =
            "SELECT id, name, description, correct_answers_percent, " +
                    "(SELECT COUNT(*) FROM question WHERE question.quiz_id=quiz.id) AS questions_count " +
                    "FROM quiz " +
                    "ORDER BY id DESC " +
                    "LIMIT " + Quiz.LAST_QUIZZES_COUNT;
    private static final String FIND_ALL_QUIZZES_QUERY =
            "SELECT id, name, description, correct_answers_percent, " +
                    "(SELECT COUNT(*) FROM question WHERE question.quiz_id=quiz.id) AS questions_count " +
                    "FROM quiz " +
                    "ORDER BY id";
    private static final String FIND_QUIZ_BY_ID =
            "SELECT id, name, description, correct_answers_percent, " +
                    "(SELECT COUNT(*) FROM question WHERE question.quiz_id=quiz.id) AS questions_count " +
                    "FROM quiz " +
                    "WHERE id = ? " +
                    "ORDER BY id " +
                    "LIMIT 1";

    private Integer id;
    private String name;
    private String description;
    private Integer correctAnswersPercent;
    private Integer questionsCount;
    private DatabaseConnection dbc;

    public Quiz() throws SQLException {
        dbc = DatabaseConnection.getInstance();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getCorrectAnswersPercent() {
        return correctAnswersPercent;
    }

    public Integer getQuestionsCount() {
        return questionsCount;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCorrectAnswersPercent(Integer correctAnswersPercent) {
        this.correctAnswersPercent = correctAnswersPercent;
    }

    protected static Quiz createFromResultSet(ResultSet rs) throws SQLException {
        Quiz quiz = new Quiz();
        quiz.id = rs.getInt("id");
        quiz.name = rs.getString("name");
        quiz.description = rs.getString("description");
        quiz.correctAnswersPercent = rs.getInt("correct_answers_percent");
        quiz.questionsCount = rs.getInt("questions_count");

        return quiz;
    }

    public static ArrayList<Quiz> getLast() throws SQLException {
        ArrayList<Quiz> quizzes = new ArrayList<>();
        PreparedStatement smtp = DatabaseConnection.getInstance().getConnection()
                .prepareStatement(FIND_LAST_QUIZZES_QUERY);
        ResultSet rs = smtp.executeQuery();
        while (rs.next()) {
            quizzes.add(Quiz.createFromResultSet(rs));
        }
        rs.close();
        smtp.close();
        return quizzes;
    }

    public static List<Quiz> findAll() throws SQLException {
        List<Quiz> quizzes = new ArrayList<>();
        PreparedStatement smtp = DatabaseConnection.getInstance().getConnection()
                .prepareStatement(FIND_ALL_QUIZZES_QUERY);
        ResultSet rs = smtp.executeQuery();
        while (rs.next()) {
            quizzes.add(Quiz.createFromResultSet(rs));
        }
        rs.close();
        smtp.close();
        return quizzes;
    }

    public static Quiz findQuizById(Integer id) throws SQLException {
        Quiz quiz = null;
        PreparedStatement stmt = DatabaseConnection.getInstance().getConnection()
                .prepareStatement(FIND_QUIZ_BY_ID);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            quiz = Quiz.createFromResultSet(rs);
        } else {
            throw new SQLException("Quiz with Id=\"" + id + "\" not found");
        }
        rs.close();
        stmt.close();
        return quiz;
    }

    public void save() throws SQLException {
        PreparedStatement stmt = dbc.getConnection().prepareStatement("INSERT INTO quiz (name, description, correct_answers_percent) VALUES (?, ?, ?)");
        stmt.setString(1, this.name);
        stmt.setString(2, this.description);
        stmt.setInt(3, this.correctAnswersPercent);
        stmt.execute();
    }
}