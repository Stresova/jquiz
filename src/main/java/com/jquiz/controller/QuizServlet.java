package com.jquiz.controller;

import com.jquiz.form.QuizForm;
import com.jquiz.model.Quiz;
import com.jquiz.service.Auth;
import com.jquiz.service.Router;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuizServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = Router.getActionName(request);
        switch (action) {
            case "list":
                listQuizzes(request, response);
                break;
            case "create":
                createQuizForm(request, response);
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        String action = Router.getActionName(request);
        switch (action) {
            case "create":
                processCreateQuiz(request, response);
                break;
            case "edit":
                editQuizForm(request, response);
                break;


            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                break;
        }
    }

    /** Actions */

    private void listQuizzes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Quiz> quizList = new ArrayList<>();
        HashMap<String,String> errorsMap = new HashMap<>();
        try {
            quizList = Quiz.findAll();
        } catch (SQLException e) {
            errorsMap.put("SQL Error", "Error occurred during quizzes selection!");
        }
        request.setAttribute("page_title", "Explore Quizzes");
        request.setAttribute("quizList", quizList);
        request.setAttribute("errorsMap", errorsMap);
        request.getRequestDispatcher("/quizzesList.jsp").forward(request, response);
    }

    private void createQuizForm(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        if (!Auth.isLoggedAsEditor(session.getId())) {
            String contextPath = request.getContextPath();
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/"));
            return;
        }

        request.setAttribute("page_title", "Create quiz");
        request.setAttribute("form", QuizForm.emptyForm());
        request.getRequestDispatcher("/quizCreateForm.jsp").forward(request, response);
    }

    private void editQuizForm(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String contextPath = request.getContextPath();
        if (!Auth.isLoggedAsEditor(session.getId())) {
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/"));
            return;
        }
        QuizForm form = null;
        try {
            Integer quizId = Integer.valueOf(request.getParameter("quizId"));
            Quiz quiz = Quiz.findQuizById(quizId);
            form = QuizForm.fromObject(quiz);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/"));
            return;
        }

        request.setAttribute("page_title", "Edit quiz");
        request.setAttribute("form", form);
        request.getRequestDispatcher("/quizEditForm.jsp").forward(request, response);
    }

    private void processCreateQuiz(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        if (!Auth.isLoggedAsEditor(session.getId())) {
            String contextPath = request.getContextPath();
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/"));
            return;
        }

        QuizForm form = QuizForm.fromRequest(request);
        form.validate();

        Quiz quiz;

        if (!form.hasErrors()) {
            try{
                quiz = new Quiz();
                quiz.setName(form.getValue("quizTitle"));
                quiz.setDescription(form.getValue("quizDescription"));
                quiz.setCorrectAnswersPercent(Integer.valueOf(form.getValue("passingScore")));
                quiz.save();

                String contextPath = request.getContextPath();
                response.sendRedirect(response.encodeRedirectURL(contextPath + "/quiz/list"));
                return;
            } catch (Exception e) {
                form.setError("general", e.getMessage());
            }
        } else {
            form.setError("general", "Error occurred");
        }

        // If errors were occurred
        request.setAttribute("page_title", "Create quiz");
        request.setAttribute("form", form);
        request.getRequestDispatcher("/quizCreateForm.jsp").forward(request, response);
    }
}