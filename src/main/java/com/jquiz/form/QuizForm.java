package com.jquiz.form;

import com.jquiz.model.Quiz;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Login form
 */
public class QuizForm extends BaseForm {

    @Override
    public ArrayList<String> getFieldsList() {
        ArrayList<String> fields = new ArrayList<>();
        fields.add("quizTitle");
        fields.add("quizDescription");
        fields.add("passingScore");
        return fields;
    }

    // Return empty form for initial login
    public static QuizForm emptyForm() {
        QuizForm form = new QuizForm();
        form.setValue("quizTitle", "");
        form.setValue("quizDescription", "");
        form.setValue("passingScore", "");
        return form;
    }

    // Use in QuizServlet > doPost() > processCreateQuiz()
    public static QuizForm fromRequest(HttpServletRequest req) {
        QuizForm form = new QuizForm();
        form.setValue("quizTitle", req.getParameter("quizTitle"));
        form.setValue("quizDescription", req.getParameter("quizDescription"));
        form.setValue("passingScore", req.getParameter("passingScore"));
        return form;
    }

    public static QuizForm fromObject(Object obj) throws Exception {
        if (!(obj instanceof Quiz)) {
            throw new Exception("Required instance of Quiz");
        }

        QuizForm form = new QuizForm();
        form.setValue("quizTitle", ((Quiz)obj).getName());
        form.setValue("quizDescription", ((Quiz)obj).getDescription());
        form.setValue("passingScore", String.valueOf(((Quiz)obj).getCorrectAnswersPercent()));
        form.setValue("password", "");
        return form;
    }

    // Use in MemberServlet > doPost() > processLogin() to fill errors map
    @Override
    public void validate() {
        String quizTitle = values.get("quizTitle").trim();
        if (quizTitle.equals("")) {
            errors.put("quizTitle", "Required");
        } else if (quizTitle.length() > 255) {
            errors.put("quizTitle", "Length is too big");
        }

        String quizDescription = values.get("quizDescription").trim();
        if (quizDescription.equals("")) {
            errors.put("quizDescription", "Required");
        } else if (quizDescription.length() > 255) {
            errors.put("quizDescription", "Length is too big");
        }

        String passingScore = values.get("passingScore").trim();
        Integer passingScoreInteger = 0;

        if (passingScore.equals("")) {
            errors.put("passingScore", "Required");
            return;
        }
        try {
            passingScoreInteger = Integer.parseInt(passingScore);
        } catch (Exception e) {
            errors.put("passingScore", "Wrong format");
            return;
        }
        if (passingScoreInteger < 1 || passingScoreInteger > 100) {
            errors.put("passingScore", "Should be in range [1 - 100]");
        }
    }
    
}
