package com.jquiz.form;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Register form
 */
public class RegisterForm extends BaseForm {

    @Override
    public ArrayList<String> getFieldsList() {
        ArrayList<String> fields = new ArrayList<>();
        fields.add("username");
        fields.add("email");
        fields.add("password");
        fields.add("passwordConfirm");
        fields.add("firstName");
        fields.add("lastName");
        return fields;
    }

    // Return empty form for initial register
    public static RegisterForm emptyForm() {
        RegisterForm form = new RegisterForm();
        form.setValue("username", "");
        form.setValue("email", "");
        form.setValue("password", "");
        form.setValue("passwordConfirm", "");
        form.setValue("firstName", "");
        form.setValue("lastName", "");
        return form;
    }

    // Use in MemberServlet > doPost() > processRegister()
    public static RegisterForm fromRequest(HttpServletRequest req) {
        RegisterForm form = new RegisterForm();
        form.setValue("username", req.getParameter("username"));
        form.setValue("email", req.getParameter("email"));
        form.setValue("password", req.getParameter("password"));
        form.setValue("passwordConfirm", req.getParameter("passwordConfirm"));
        form.setValue("firstName", req.getParameter("firstName"));
        form.setValue("lastName", req.getParameter("lastName"));

        return form;
    }

    // Use in MemberServlet > doPost() > processRegister() to fill errors map
    @Override
    public void validate() {
        // Validate username
        String username = values.get("username").trim();
        if (username.equals("")) {
            errors.put("username", "Required");
        } else if (username.length() > 30) {
            errors.put("username", "Length is too big");
        } else if (!username.matches("^[a-z0-9]+$")) {
            errors.put("username", "Incorrect format");
        }

        // Validate email
        String email = values.get("email").trim();
        if (email.equals("")) {
            errors.put("email", "Required");
        } else if (email.length() > 60) {
            errors.put("email", "Length is too big");
        } else if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            //pattern from http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
            errors.put("email", "Incorrect format");
        }

        // Validate password
        String password = values.get("password").trim();
        if (password.equals("")) {
            errors.put("password", "Required");
        } else if (password.length() > 30) {
            errors.put("password", "Length is too big");
        }

        // Validate passwordConfirm
        String passwordConfirm = values.get("passwordConfirm").trim();
        if (!passwordConfirm.equals(password)) {
            errors.put("password", "Passwords are not equals");
        } else if (passwordConfirm.equals("")) {
            errors.put("passwordConfirm", "Required");
        } else if (passwordConfirm.length() > 30) {
            errors.put("passwordConfirm", "Length is too big");
        }

        // Validate firstName
        String firstName = values.get("firstName").trim();
        if (firstName.equals("")) {
            errors.put("firstName", "Required");
        } else if (firstName.length() > 30) {
            errors.put("firstName", "Length is too big");
        }  else if (!firstName.matches("^[a-zA-Z\\p{L} ,.'-]+$")) {
           errors.put("firstName", "Incorrect format");
        }

        // Validate lastName
        String lastName = values.get("lastName").trim();
        if (lastName.equals("")) {
            errors.put("lastName", "Required");
        } else if (lastName.length() > 30) {
            errors.put("lastName", "Length is too big");
        } else if (!lastName.matches("^[a-zA-Z\\p{L} ,.'-]+$")) {
            errors.put("lastName", "Incorrect format");
        }

    }


}