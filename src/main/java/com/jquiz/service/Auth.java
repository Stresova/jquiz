package com.jquiz.service;

import javax.servlet.ServletException;
import com.jquiz.model.Member;

/**
 * Tools for manage authentication
 */
public class Auth {

    public static void createAuth(Member member, String sessionId) throws ServletException{
        MemberSession session = MemberSession.getInstance();
        session.putObject("member:" + sessionId, member);
    }

    // TODO: research how to escape from pass "sessionId" to each method
    
    public static void destroyAuth(String sessionId) {
        MemberSession session = MemberSession.getInstance();
        session.removeObject("member:" + sessionId);
    }

    public static Member getCurrentMember(String sessionId) {
        MemberSession session = MemberSession.getInstance();
        return (Member)session.getObject("member:" + sessionId);
    }

    public static boolean isLogged(String sessionId) {
        MemberSession session = MemberSession.getInstance();
        Member member = getCurrentMember(sessionId);
        if (member != null) session.updateActivity(sessionId);
        return member != null;
    }

    public static boolean isLoggedAsEditor(String sessionId) {
        Member member = getCurrentMember(sessionId);
        if (member == null) return false;
        return member.role == Member.Role.Editor;
    }
    
    public static String getMemberUsername(String sessionId) {
        Member member = getCurrentMember(sessionId);
        return member == null ? "" : member.username;
    }
}
