<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<footer class="footer">
    <div class="container">
        <p class="text-muted" style="float: left;">&copy; 2015 <a href="<c:out value="${base_url}"/>">JQuiz</a>.</p>
        <p class="text-muted" style="float: right;"><a href="https://bitbucket.org/brevis/jquiz">Bitbucket</a></p>
    </div>
</footer>