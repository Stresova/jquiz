<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="members" scope="request" type="java.util.ArrayList<com.jquiz.model.Member>"/>
<jsp:useBean id="page_title" scope="request" type="java.lang.String"/>
<jsp:include page="_header.jsp" />
<div class="container">
    <h1><c:out value="${page_title}"/></h1>
    <% if (members.size() > 0) {%>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="member" items="${members}" varStatus="counter">
                    <tr>
                        <td><c:out value="${counter.count}"/></td>
                        <td><c:out value="${member.firstName}"/></td>
                        <td><c:out value="${member.lastName}"/></td>
                        <td><c:out value="${member.role}"/></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    <% } else { %>
        <p>No members found :(</p>
    <% } %>
</div>
<jsp:include page="_footer.jsp" />