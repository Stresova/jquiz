<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://example.com/functions" prefix="f" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="quizList" scope="request" type="java.util.List<com.jquiz.model.Quiz>"/>
<jsp:useBean id="page_title" scope="request" type="java.lang.String"/>
<jsp:useBean id="errorsMap" scope="request" type="java.util.HashMap<java.lang.String,java.lang.String>"/>
<jsp:include page="_header.jsp" />
<c:set var="isLogged" value="${f:isLogged(pageContext.session.id)}"/>
<c:set var="isLoggedAsEditor" value="${f:isLoggedAsEditor(pageContext.session.id)}"/>
<div class="container">
    <c:forEach var="error" items="${errorsMap}" varStatus="counter">
        <div class="alert alert-danger" role="alert"><c:out value="${error.key}: ${error.value}"/></div>
    </c:forEach>
    <h1><c:out value="${page_title}"/></h1>
    <c:if test="${!isLogged}">
        <div class="alert alert-warning" role="alert"><strong>Warning!</strong> To start quiz you must <strong><a href="<c:out value="${base_url}"/>member/login">Login</a></strong>.</div>
    </c:if>
    <c:if test="${isLoggedAsEditor}">
        <div class="jumbotron" style="text-align:center;">
            <a href="<c:out value="${base_url}"/>quiz/create" class="btn btn-primary btn-lg" role="button">Create new quiz</a>
        </div>
    </c:if>
    <div class="row">
        <c:choose>
            <c:when test="${fn:length(quizList) > 0}">
                <c:forEach var="quiz" items="${quizList}" varStatus="counter">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix"><strong><c:out value="${quiz.name}"/></strong>
                             <span class="pull-right">
                                <span class="info-label" data-toggle="tooltip" data-placement="top" title="Number of questions">
                                   <span class="glyphicon glyphicon-list-alt"></span>
                                   <span class="info-text"><c:out value="${quiz.questionsCount}"/></span>
                                </span>
                                <span class="info-label"  data-toggle="tooltip" data-placement="top" title="Passing score">
                                   <span class="glyphicon glyphicon-thumbs-up"></span>
                                   <span class="info-text"><c:out value="${quiz.correctAnswersPercent}"/>%</span>
                                </span>
                             </span>
                            </div>
                            <div class="panel-body">
                                <c:out value="${quiz.description}"/>
                            </div>
                            <div class="panel-footer">
                                <div style="display: inline-block" class="<c:out value="${! isLogged ? 'tooltip-wrapper': ''}"/>"
                                     data-html="true"
                                     data-placement="top"
                                     data-content='To start quiz you must <strong><a href="<c:out value="${base_url}"/>member/login">Login</a></strong>.'
                                     >
                                    <a href="" class="btn btn-primary <c:out value="${! isLogged ? 'disabled': ''}"/>"
                                       role="button">
                                        <span class="glyphicon glyphicon-play" aria-hidden="true"></span>
                                        Start
                                    </a>
                                </div>
                                <c:if test="${isLoggedAsEditor}">
                                    <span class="pull-right">
                                        <form action="<c:out value="${base_url}"/>quiz/edit" method="post">
                                            <input type="hidden" name="quizId" value="${quiz.id}">
                                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button>
                                        </form>
                                        <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</button>
                                    </span>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <c:if test="${(counter.count % 2) == 0}">
                        </div>
                        <div class="row">
                    </c:if>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <h2>There are not any quizzes yet.</h2>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(function () {
        $(".tooltip-wrapper").popover({
            title : '<span class="text-danger"><strong>Warning!</strong></span><button type="button" id="close" class="close" onclick="$(&quot;.tooltip-wrapper&quot;).popover(&quot;hide&quot;);">&times;</button>'
        });
    })
</script>

<jsp:include page="_footer.jsp" />
