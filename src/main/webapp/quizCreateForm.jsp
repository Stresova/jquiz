<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:useBean id="page_title" scope="request" type="java.lang.String"/>
<jsp:useBean id="form" scope="request" type="com.jquiz.form.QuizForm"/>
<jsp:include page="_header.jsp" />
    <!---------------------------------------------------->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <h1><c:out value="${page_title}"/></h1>
                <form action="<c:out value="${base_url}"/>quiz/create" method="post">
                    <% if (form.hasError("general")) { %>
                        <div class="alert alert-danger">${fn:escapeXml(form.getError("general"))}</div>
                    <% } %>

                    <div class="form-group <% if (form.hasError("quizTitle")) { %> has-error<% } %>">
                        <label for="quizTitle"><strong>Title:</strong></label>
                        <input type="text" class="form-control" id="quizTitle" name="quizTitle" placeholder="Title" required maxlength="255" value="${fn:escapeXml(form.getValue("quizTitle"))}">
                        <p class="help-block">${fn:escapeXml(form.getError("quizTitle"))}</p>
                    </div>

                    <div class="form-group <% if (form.hasError("quizDescription")) { %> has-error<% } %>">
                        <label for="quizDescription"><strong>Description:</strong></label>
                        <textarea class="form-control" id="quizDescription" name="quizDescription" rows="3" required maxlength="255">${fn:escapeXml(form.getValue("quizDescription"))}</textarea>
                        <p class="help-block">${fn:escapeXml(form.getError("quizDescription"))}</p>
                    </div>

                    <div class="form-group <% if (form.hasError("passingScore")) { %> has-error<% } %>">
                        <label for="passingScore"><strong>Passing score:</strong></label>
                         <span id="passingScoreHelp"
                               class="glyphicon glyphicon-question-sign"
                               aria-hidden="true"
                               data-toggle="popover"
                               data-content="The percentage of correct answers required to pass the quiz"
                               data-trigger="hover"
                               data-placement="top"></span>
                        <div class="row">
                            <div class="col-xs-9 col-sm-10">
                                <div id="slider"></div>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <input type="text" class="form-control" id="passingScore" name="passingScore" required value="${fn:escapeXml(form.getValue("passingScore"))}" />
                            </div>
                        </div>
                        <p class="help-block">${fn:escapeXml(form.getError("passingScore"))}</p>
                    </div>

                    <div class="col-xs-12 text-center">
                        <button type="submit" id="create-quiz-button" class="btn btn-primary btn-lg">Create quiz</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script>

    // On document ready, initialize noUiSlider.
    $(function(){

        $('#slider').noUiSlider({
            start: [70],
            step: 1,
            range: {
                'min': 1,
                'max': 100
            },

            format: {
                to: function ( value ) {
                    return Math.round( value );
                },
                from: function ( value ) {
                    return value;
                }
            }

        });

        $("#slider").Link('lower').to($('#passingScore'));
    });

    $(function () {
        $('[data-toggle="popover"]').popover()
    })

</script>

<jsp:include page="_footer.jsp" />