<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="lastQuizzesList" scope="request" type="java.util.ArrayList<com.jquiz.model.Quiz>"/>
<jsp:useBean id="errorsMap" scope="request" type="java.util.HashMap<java.lang.String,java.lang.String>"/>

<jsp:include page="_header.jsp" />
<div class="container">
    <c:forEach var="error" items="${errorsMap}" varStatus="counter">
        <div class="alert alert-danger" role="alert"><c:out value="${error.key}: ${error.value}"/></div>
    </c:forEach>
    <div class="jumbotron" style="text-align: center;">
        <h1>JQuiz</h1>
        <p>(real content coming soon)</p>
    </div>
    <div class="row">
        <c:choose>
            <c:when test="${fn:length(lastQuizzesList) > 0}">
                <c:forEach var="quiz" items="${lastQuizzesList}" varStatus="counter">
                    <c:choose>
                        <c:when test="${counter.count % 3 == 0}">
                            <c:set var="columnStyle" value="col-sm-12"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="columnStyle" value="col-sm-6"/>
                        </c:otherwise>
                    </c:choose>
                    <div class="col-xs-12 <c:out value="${columnStyle}"/> col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><c:out value="${quiz.name}"/></h3>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item"><span class="badge"><c:out value="${quiz.questionsCount}"/></span><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Questions</li>
                            </ul>
                            <div class="panel-body">
                                <c:out value="${quiz.description}"/>
                            </div>
                            <div class="panel-footer">
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> Start quiz</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <c:if test="${(counter.count % 3) == 0}">
                        </div>
                        <div class="row">
                    </c:if>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <h2>There are not any quizzes yet.</h2>
            </c:otherwise>
        </c:choose>
    </div>
</div>
<jsp:include page="_footer.jsp" />