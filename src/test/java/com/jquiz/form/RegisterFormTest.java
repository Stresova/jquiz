package com.jquiz.form;

import com.jquiz.form.RegisterForm;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RegisterFormTest {

    /** form common */

    @Test
    public void emptyForm() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.validate();

        assertTrue(form.hasErrors());
        assertTrue(form.hasError("username"));
        assertTrue(form.hasError("email"));
        assertTrue(form.hasError("password"));
        assertTrue(form.hasError("passwordConfirm"));
        assertTrue(form.hasError("firstName"));
        assertTrue(form.hasError("lastName"));
    }

    @Test
    public void formOk() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("username", "username");
        form.setValue("email", "name@domain.tld");
        form.setValue("password", "password");
        form.setValue("passwordConfirm", "password");
        form.setValue("firstName", "John");
        form.setValue("lastName", "Doe");
        form.validate();

        assertFalse(form.hasErrors());
    }

    /** username */

    @Test
    public void usernameOk() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("username", "username");
        form.validate();

        assertFalse(form.hasError("username"));
    }

    @Test
    public void usernameEmpty() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("username"));
        assertEquals(form.getError("username"), "Required");
    }

    @Test
    public void usernameIncorrectFormat() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("username", "!@#$%^");
        form.validate();

        assertTrue(form.hasError("username"));
        assertEquals(form.getError("username"), "Incorrect format");
    }

    @Test
    public void usernameIncorrectLength() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("username", "qwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("username"));
        assertEquals(form.getError("username"), "Length is too big");
    }

    /** email */

    @Test
    public void emailOk() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("email", "name@domain.tld");
        form.validate();
        assertFalse(form.hasError("email"));
    }

    @Test
    public void emailEmpty() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("email"));
        assertEquals(form.getError("email"), "Required");
    }

    @Test
    public void emailIncorrectFormat() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("email", "email@=email");
        form.validate();

        assertTrue(form.hasError("email"));
        assertEquals(form.getError("email"), "Incorrect format");
    }

    @Test
    public void emailIncorrectLength() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("email", "mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail@mail.com");
        form.validate();

        assertTrue(form.hasError("email"));
        assertEquals(form.getError("email"), "Length is too big");
    }

    /** passwords */

    @Test
    public void passwordOk() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("password", "password");
        form.setValue("passwordConfirm", "password");
        form.validate();

        assertFalse(form.hasError("password"));
    }

    @Test
    public void passwordEmpty() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("password"));
        assertEquals(form.getError("password"), "Required");
        assertTrue(form.hasError("passwordConfirm"));
        assertEquals(form.getError("passwordConfirm"), "Required");
    }

    @Test
    public void passwordsNotEquals() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("password", "password1");
        form.setValue("passwordConfirm", "password2");
        form.validate();

        assertTrue(form.hasError("password"));
        assertEquals(form.getError("password"), "Passwords are not equals");
    }

    @Test
    public void passwordsIncorrectLength() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("password", "qwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.setValue("passwordConfirm", "qwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("password"));
        assertEquals(form.getError("password"), "Length is too big");
        assertTrue(form.hasError("passwordConfirm"));
        assertEquals(form.getError("passwordConfirm"), "Length is too big");
    }

    /** firstName */

    @Test
    public void firstNameOk() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("firstName", "John");
        form.validate();

        assertFalse(form.hasError("firstName"));
    }

    @Test
    public void firstNameEmpty() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("firstName"));
        assertEquals(form.getError("firstName"), "Required");
    }

    @Test
    public void firstNameIncorrectLength() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("firstName", "qwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("firstName"));
        assertEquals(form.getError("firstName"), "Length is too big");
    }

    @Test
    public void firstNameIncorrectFormat() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("firstName", "user]");

        form.validate();

        assertTrue(form.hasError("firstName"));
        assertEquals(form.getError("firstName"), "Incorrect format");
    }

    @Test
    public void firstNameCorrectFormat() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        String[] validNames = { "Mathias d'Arras",
                                "Martin Luther King, Jr.",
                                "Hector Sausage-Hausen",
                                "Иван"};
        for (String validName : validNames) {
            form.setValue("firstName", validName);
            form.validate();
        }
        assertFalse(form.hasError("firstName"));
        assertFalse(form.getError("firstName").equals("Incorrect format"));
    }

    /** lastName */

    @Test
    public void lastNameOk() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("lastName", "Doe");
        form.validate();

        assertFalse(form.hasError("lastName"));
    }

    @Test
    public void lastNameEmpty() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("lastName"));
        assertEquals(form.getError("lastName"), "Required");
    }

    @Test
    public void lastNameIncorrectLength() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("lastName", "qwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("lastName"));
        assertEquals(form.getError("lastName"), "Length is too big");
    }

    @Test
    public void lastNameIncorrectFormat() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        form.setValue("lastName", "user]");

        form.validate();

        assertTrue(form.hasError("lastName"));
        assertEquals(form.getError("lastName"), "Incorrect format");
    }

    @Test
    public void lastNameCorrectFormat() {
        com.jquiz.form.RegisterForm form = com.jquiz.form.RegisterForm.emptyForm();
        String[] validNames = {"Mathias d'Arras",
                "Martin Luther King, Jr.",
                "Hector Sausage-Hausen",
                "Иван"};
        for (String validName : validNames) {
            form.setValue("lastName", validName);
            form.validate();
        }
        assertFalse(form.hasError("lastName"));
        assertFalse(form.getError("lastName").equals("Incorrect format"));
    }
}